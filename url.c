/* Dooba SDK
 * URL Manipulation
 */

// External Includes
#include <stdlib.h>
#include <string.h>
#include <util/str.h>

// Internal Includes
#include "url.h"

// Split URL
void url_split(struct url *u, char *url)
{
	// Split
	url_split_n(u, url, strlen(url));
}

// Split URL - Fixed Length
void url_split_n(struct url *u, char *url, uint16_t url_len)
{
	int16_t x;
	uint16_t endp_s;
	uint16_t endp_l;

	// Set String
	u->str = url;

	// Extract Protocol
	endp_s = 0;
	u->proto_l = 0;
	x = str_find(url, url_len, "://", &endp_s);
	if(x > 0)																	{ u->proto_l = x; }

	// Extract Network Endpoint (Host + Port)
	endp_l = url_len - endp_s;
	x = str_find(&(url[endp_s]), endp_l, "/", 0);
	if(x >= 0)																	{ endp_l = x; }

	// Extract Host & Port from Endpoint
	u->port = 0;
	u->host_s = endp_s;
	u->host_l = endp_l;
	x = str_find(&(url[endp_s]), endp_l, ":", 0);
	if(x >= 0)
	{
		// Set Host Length
		u->host_l = x;

		// Extract Port
		u->port = strtoul(&(url[u->host_s + u->host_l + 1]), 0, 10);
	}

	// Extract Path
	u->path_s = endp_s + endp_l;
	u->path_l = url_len - u->path_s;
}
