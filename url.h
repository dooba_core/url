/* Dooba SDK
 * URL Manipulation
 */

#ifndef	__URL_H
#define	__URL_H

// External Includes
#include <stdint.h>
#include <avr/io.h>

// URL Shortcuts
#define	url_proto(url)										((char *)((url)->str))
#define	url_host(url)										((char *)(&((url)->str[(url)->host_s])))
#define	url_path(url)										((char *)(&((url)->str[(url)->path_s])))

// URL Structure
struct url
{
	// URL String
	char *str;

	// Protocol
	uint16_t proto_l;

	// Host
	uint16_t host_s;
	uint16_t host_l;

	// Port
	uint16_t port;

	// Path
	uint16_t path_s;
	uint16_t path_l;
};

// Split URL
extern void url_split(struct url *u, char *url);

// Split URL - Fixed Length
extern void url_split_n(struct url *u, char *url, uint16_t url_len);

#endif
